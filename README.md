# docker
https://docs.docker.com/engine/install/debian/
https://docs.docker.com/engine/reference/commandline/docker/

https://phoenixnap.com/kb/how-to-ssh-into-docker-container
https://awesomeopensource.com/project/sam46/Agar.io-Clone

# * * * * * * * * * * * * * #
#Docker debian install with apt

sudo apt update  && sudo apt upgrade

sudo apt install ca-certificates curl gnupg

sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo docker run hello-world

Post installation:
sudo groupadd docker
sudo usermod -aG docker $USER
exec su -l $USER
docker run hello-world
docker ps -n 1

post installation notes:
docker exec command
https://www.digitalocean.com/community/tutorials/how-to-use-docker-exec-to-run-commands-in-a-docker-container
